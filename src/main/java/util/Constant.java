/**
 * 
 */
package util;

import org.atmosphere.cpr.BroadcasterFactory;

/**
 * @author Shawn
 */
public final class Constant {
	public static final String BROADCASTER_FACTORY_KEY = BroadcasterFactory.class.getName() + "_key";
	
	private Constant() {
	}
}