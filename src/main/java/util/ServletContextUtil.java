/**
 * 
 */
package util;

import static util.Constant.BROADCASTER_FACTORY_KEY;

import javax.servlet.ServletContext;

import org.atmosphere.cpr.BroadcasterFactory;
import org.springframework.web.context.ServletContextAware;

/**
 * @author Shawn
 */
public final class ServletContextUtil implements ServletContextAware {
	private static final ServletContextUtil instance = new ServletContextUtil();
	
	private ServletContext servletContext;
	
	private ServletContextUtil() {
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	public static ServletContextUtil getInstance() {
		return instance;
	}
	
	public static void putContextVariable(String key, Object value) {
		getServletContext().setAttribute(key, value);
	}
	
	public static Object getContextVariable(String key) {
		return getServletContext().getAttribute(key);
	}

	public static BroadcasterFactory getBroadcasterFactory() {
		return (BroadcasterFactory) getContextVariable(BROADCASTER_FACTORY_KEY);
	}
	
	private static ServletContext getServletContext() {
		return instance.servletContext;
	}

}