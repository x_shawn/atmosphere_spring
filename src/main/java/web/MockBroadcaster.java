/**
 * 
 */
package web;

import java.util.Date;

import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;

import util.ServletContextUtil;

/**
 * @author Shawn
 */
public class MockBroadcaster implements Runnable {
	private static final String USER_ID = "Shawn";

	@Override
	public void run() {
		BroadcasterFactory bcFactory = ServletContextUtil.getBroadcasterFactory();
		Broadcaster broadcaster = bcFactory.lookup(USER_ID);
		
		for(int i = 0; i < 10; i++){
			Data event = new Data("author_" + i, "message_" + i);
			try {
				Thread.sleep(3 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			broadcaster.broadcast(event);
		}
	}

	public final static class Data {

		private final String text;
		private final String author;

		public Data(String author, String text) {
			this.author = author;
			this.text = text;
		}

		public String toString() {
			return "{ \"text\" : \"" + text + "\", \"author\" : \"" + author
					+ "\" , \"time\" : " + new Date().getTime() + "}";
		}

	}
}