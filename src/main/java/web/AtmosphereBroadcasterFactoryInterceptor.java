/**
 * 
 */
package web;

import static util.Constant.BROADCASTER_FACTORY_KEY;

import org.atmosphere.cpr.Action;
import org.atmosphere.cpr.AtmosphereConfig;
import org.atmosphere.cpr.AtmosphereInterceptor;
import org.atmosphere.cpr.AtmosphereResource;

import util.ServletContextUtil;

/**
 * @author Shawn
 */
public class AtmosphereBroadcasterFactoryInterceptor implements
		AtmosphereInterceptor {

	@Override
	public void configure(AtmosphereConfig config) {
		if(ServletContextUtil.getBroadcasterFactory() == null){
			ServletContextUtil.putContextVariable(
					BROADCASTER_FACTORY_KEY, config.getBroadcasterFactory());
		}
	}

	@Override
	public Action inspect(AtmosphereResource r) {
		return Action.CONTINUE;
	}

	@Override
	public void postInspect(AtmosphereResource r) {
	}
}