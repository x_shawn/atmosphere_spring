package web;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import util.ServletContextUtil;

/**
 * Atmosphere Spring MVC integration Demo; note that
 * 
 * 1. to enable the original demo of 'chat' app, disable the current 'onHandshaking'; instead, 
 * enable the commented 'onRequest' and 'onPost'; further, change the URI in request JSON of app.js
 */
@Controller
public class AtmosphereController {
	// to log how many subscribers from the client side
	/*private static final Collection<HttpSession> subscribers = Collections.synchronizedCollection(
			new ArrayList<HttpSession>());*/

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getView() {
		return new ModelAndView("chat");
	}

	/**
	 * Mock 1 broadcaster per user; note that
	 * 
	 * 1. BroadcasterFactory is initialized and cached in the whole application scope for later use, 
	 * e.g. service event or WS event
	 */
	@RequestMapping(value = "/chat/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public void onHandshaking(@PathVariable("userId") String userId, HttpServletRequest request) {
		
		// Atmosphere framework puts filter/servlet that adds ATMOSPHERE_RESOURCE to all requests
	    AtmosphereResource resource = (AtmosphereResource) request.getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);

		// handshaking
	    resource.resumeOnBroadcast(
	    		resource.transport() == AtmosphereResource.TRANSPORT.LONG_POLLING)
				.suspend();
		
		// associate a Broadcaster with the current AtmosphereResource
	    BroadcasterFactory factory = ServletContextUtil.getBroadcasterFactory();
		Broadcaster broadcaster = factory.lookup(userId, true);
		broadcaster.addAtmosphereResource(resource);
		
		// mock the Broadcaster.broadcast operation in a separate thread
		ExecutorService singleThreadPool = Executors.newSingleThreadExecutor();
		singleThreadPool.execute(new MockBroadcaster());
	}

	/**
	 * A simple handshaking demo; note that 
	 * 
	 * 1. an AtmosphereResource represents the current connection with client
	 * (the requested protocol could be 'WebSocket', 'LongPolling', 'SSE', etc.); 
	 * 
	 * 2. by default, an AtmosphereResource is identified via UUID - that is, each different connection 
	 * requested by the different client (different Browser window/tab) is assigned with the different UUID
	 * 
	 * 3. current atmosphereResource is always associated with its Broadcaster 
	 * (a new one will be created if not existed); by default, each different atmosphereResource shares
	 * the same Broadcaster
	 * 
	 * 4. by convention, current connection should be resumed after each Broadcaster.broadcast operation 
	 * in 'LongPolling' case; for 'WebSocket', we keep it open
	 */
/*
	@RequestMapping(value = "/chat", method = RequestMethod.GET)
	public void onRequest(AtmosphereResource atmosphereResource, HttpSession session) 
			throws IOException {
		
		AtmosphereRequest atmosphereRequest = atmosphereResource.getRequest();
		System.out.println(atmosphereRequest.getHeader("negotiating"));
		if (atmosphereRequest.getHeader("negotiating") == null) {
			atmosphereResource.resumeOnBroadcast(
							atmosphereResource.transport() == AtmosphereResource.TRANSPORT.LONG_POLLING)
					.suspend();
		} else {
			atmosphereResource.getResponse().getWriter().write("OK");
		}

		subscribers.add(session);
		System.out.println("Subscribers: " + subscribers.size());
		for (HttpSession httpSession : subscribers) {
			System.out.println(httpSession);
		}
	}
*/
	/**
	 * Message exchanging handler when the client use the same connection to push message ('WebSocket' is a bi-direction)
	 * 
	 * Note that, since here Broadcaster is associated with every atmosphereResource, even when only current atmosphereResource
	 * triggers Broadcaster.broadcast action, it will be broadcasted to every other atmosphereResource as well 
	 * (e.g. 2 clients receive the same message)
	 */
/*
	@RequestMapping(value = "/chat", method = RequestMethod.POST)
	@ResponseBody
	public void onPost(AtmosphereResource atmosphereResource)
			throws IOException {

		AtmosphereRequest atmosphereRequest = atmosphereResource.getRequest();
		String body = atmosphereRequest.getReader().readLine().trim();

		String author = body.substring(body.indexOf(":") + 2,
				body.indexOf(",") - 1);
		String message = body.substring(body.lastIndexOf(":") + 2,
				body.length() - 2);

		atmosphereResource.getBroadcaster().broadcast(new Data(author, message).toString());
	}
*/
}